#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "menu.h"

#define END_OF_LIST NULL

int getMenuOption(const char *firstStr, ...)
{
    int menuOption;
    int countOpts = 0;
    printf("\n\n");
    do
    {
        va_list argptr;
        va_start(argptr, firstStr);

        const char *str = firstStr;
        while (str != END_OF_LIST) {
            countOpts++;
            printf("%d - %s\n", countOpts, str);
            str = va_arg(argptr, const char *);
        }
        va_end(argptr);

        printf("0 - to exit \n");

        int ch;
        scanf(" %d", &menuOption);
        while ((ch = getchar()) != '\n');

        if (menuOption == 0 || (menuOption >= 1 && menuOption <= countOpts))
        {
            return menuOption;
        }
        countOpts = 0;
    } while (1);
}