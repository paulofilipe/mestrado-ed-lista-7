#include <stdio.h>
#include <stdlib.h>

#include "menu.h"
#include "disjoint-set.h"

int main(void)
{
	
	int loopFlag = 1;
	int value;
	DisjointSet * set = NULL;

	while (loopFlag)
	{
		int opt = getMenuOption("Criar a estrutura de dados",
                                "Dado dois elementos x e y, fazer a união entre os dois conjuntos aos quais pertencem x e y",
                                "Recuperar o representante de um determinado elemento x",
                                "Verificar se dois elementos x e y fazem parte do mesmo conjunto",
                                "Liberar a estrutura de dados (ou seja, a coleção de conjuntos disjuntos)",
								NULL);
		printf("\nopt chose ----> %d\n", opt);

		switch (opt)
		{
			case 1:
				printf("\nDigite 'n': \n");
				value = readFromKeyBoard();
				set = makeSet(value);
				break;
			case 2:
				printf("\n");
				if (set == NULL){ printf("\nSet vazio\n"); continue;}
				itensUnion(set);
				break;
			case 3:
				printf("\n");
				if (set == NULL){ printf("\nSet vazio\n"); continue;}
				Item * i = findItemByValue(set);
				Item * representative = findRepresentative(i);
				if (representative == i) {
					printf("\no item já é a raiz do set a qual pertence\n");
				} else {
					printf("\nA raiz do item %d é %d: \n", getItemValue(i), getItemValue(representative) );
				}
				break;
			case 4:
				if (set == NULL){ printf("\nSet vazio\n"); continue;}
				verifySameSet(set);
				break;
			case 5:
				if (set == NULL){ printf("\nSet vazio\n"); continue;}
				set = freeDisjointSet(set);
				break;
			default:
				loopFlag = 0;
				break;
		}
	}
	getchar();
	return 0;
}
