#include <stdio.h>
#include <stdlib.h>

#include "disjoint-set.h"

typedef struct disjointSet DisjointSet;
typedef struct item Item;

struct item
{
    Item *parent;
    int value;
    int rank;
};

struct disjointSet
{
    int size;
    Item **itens;
};

DisjointSet *makeSet(int size)
{
    if (size < 1)
    {
        printf("\nVALOR INVÀLIDO\n");
        return NULL;
    }

    DisjointSet *set = malloc(sizeof(DisjointSet));
    set->size = size;
    set->itens = malloc(sizeof(Item *) * set->size);
    int i = 1;
    for (i = 1; i <= set->size; i++)
    {
        Item *item = malloc(sizeof(Item *));
        item->parent = item;
        item->value = i;
        item->rank = 0;
        set->itens[i] = item;
    }
    return set;
}

Item *findItemByValue(DisjointSet *set)
{
    int val;
    do {
        printf("\nDigite valor do elemento: \n");
        val = readFromKeyBoard();
    } while (val < 1 || val > getSize(set));

    return set->itens[val];
}

Item *findRepresentative(Item *item)
{
    if (item->parent != item)
        item->parent = findRepresentative(item->parent); //path compression

    return item->parent;
}

DisjointSet *freeDisjointSet(DisjointSet * set) {
    int i = 1;

    free(set);
    
    return NULL;
}

void itensUnion(DisjointSet * set)
{
    Item *item1, *item2;
    printf("\nprimeiro elemento\n");
    item1 = findItemByValue(set);

    printf("\nsegundo elemento\n");
    item2 = findItemByValue(set);

    if (item2->rank > item1->rank)
    {
        item1->parent = item2;
    }
    else if (item1->rank > item2->rank)
    {
        item2->parent = item1;
    }
    else
    {
        item2->parent = item1;
        item1->rank++;
    }
}

void verifySameSet(DisjointSet * set) {
    Item *item1, *item2;
    printf("\nprimeiro elemento\n");
    item1 = findItemByValue(set);

    printf("\nsegundo elemento\n");
    item2 = findItemByValue(set);

    if (findRepresentative(item1) == findRepresentative(item2)) {
        printf("\nItem %d e Item %d possuem o mesmo Item raiz: %d\n", 
            item1->value, item2->value, item1->parent->value);
    } else {
        printf("\nItem %d e Item %d NÃO possuem o mesmo Item raiz\n", item1->value, item2->value);
        printf("\nItem raiz do item %d é: %d\n", item1->value, findRepresentative(item1)->value);
        printf("\nItem raiz do item %d é: %d\n", item2->value, findRepresentative(item2)->value);
    }
}

int getSize(DisjointSet *set)
{
    return set->size;
}

int readFromKeyBoard()
{
	int value;
	int ch;
	scanf(" %d", &value);
	while ((ch = getchar()) != '\n')
		;
	return value;
}

int getItemValue(Item * i) {
    return i->value;
}