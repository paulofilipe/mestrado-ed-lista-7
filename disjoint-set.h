#ifndef DISJOINT_SET
#define DISJOINT_SET
typedef struct disjointSet DisjointSet;
typedef struct item Item;

DisjointSet * makeSet(int numElems);

Item * findRepresentative(Item *item);

Item * findItemByValue(DisjointSet *set);

DisjointSet * freeDisjointSet(DisjointSet * set);

int getSize(DisjointSet * set);

int readFromKeyBoard();

int getItemValue(Item * i);

void itensUnion(DisjointSet * set);

void verifySameSet(DisjointSet * set);

#endif